<?php

namespace Mocks;

class WarriorMock
{
    /**
     * @var int
     */
    protected $health;

    /**
     * @var SpaceMock
     */
    protected $feelSpace;

    /**
     * @var []SpaceMock
     */
    protected $listenSpaces;

    /**
     * @var bool
     */
    public $restInvoked = false;

    /**
     * @var bool
     */
    public $bindInvoked = false;

    /**
     * @var bool
     */
    public $rescueInvoked = false;

    /**
     * @var bool
     */
    public $walkInvoked = false;

    public function __construct(array $input)
    {
        $this->health       = $input['health'] ?? 20;
        $this->feelSpace    = $input['feelSpace'];
        $this->listenSpaces = $input['listenSpaces'] ?? [$this->feelSpace];
    }

    public function rest()
    {
        $this->restInvoked = true;
    }

    public function bind()
    {
        $this->bindInvoked = true;
    }

    public function rescue()
    {
        $this->rescueInvoked = true;
    }

    public function walk()
    {
        $this->walkInvoked = true;
    }

    public function health(): int
    {
        return $this->health;
    }

    public function listen(): array
    {
        return $this->listenSpaces;
    }

    public function feel(): SpaceMock
    {
        return $this->feelSpace;
    }

    public function direction_of(): string
    {
        return 'forward';
    }
}
