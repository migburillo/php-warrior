<?php

namespace Mocks;

class SpaceMock
{
    /**
     * @var bool
     */
    protected $isEnemy;

    /**
     * @var bool
     */
    protected $isTicking;

    public function __construct(array $input = [])
    {
        $this->isTicking = $input['isTicking'] ?? false;
        $this->isEnemy   = $input['isEnemy']   ?? false;
        $this->isCaptive = $input['isCaptive'] ?? false;
    }

    public function is_enemy()
    {
        return $this->isEnemy;
    }

    public function is_ticking()
    {
        return $this->isTicking;
    }

    public function is_captive()
    {
        return $this->isCaptive;
    }

    public function is_stairs()
    {
        return false;
    }
}
