<?php

class Player
{
    protected const BACKWARD = 'backward';
    protected const FORWARD  = 'forward';
    protected const MAX_HP   = 20;
    protected const MIN_HP   = 8;

    /**
     * @var mixed
     */
    protected $warrior;

    /**
     * @var bool
     */
    protected $resting = false;

    /**
     * @var int
     */
    protected $health;

    public function play_turn($warrior)
    {
        $this->warrior = $warrior;

        if ($warrior->feel()->is_wall() || $this->unit_ahead(self::BACKWARD, 'archer') || $this->unit_ahead(self::BACKWARD, 'captive')) {
            $warrior->pivot();
        } elseif ($warrior->feel()->is_captive()) {
            $warrior->rescue();
        } elseif ($this->unit_ahead(self::FORWARD, 'captive')) {
            $warrior->walk();
        } elseif ($this->unit_ahead(self::FORWARD)) {
            $warrior->shoot();
        } elseif ($this->unit_ahead(self::BACKWARD)) {
            $warrior->pivot();
        } elseif (!$warrior->feel()->is_empty()) {
            $warrior->attack();
        } elseif ($this->should_rest() && $this->taking_damage()) {
            $warrior->walk(self::BACKWARD);
        } elseif ($this->should_rest()) {
            $warrior->rest();
            $this->resting = !($warrior->health() >= self::MAX_HP);
        } else {
            $warrior->walk();
        }

        $this->health = $warrior->health();
    }

    protected function unit_ahead(string $direction, string $type = 'enemy'): bool
    {
        foreach ($this->warrior->look($direction) as $space) {
            if ('enemy' == $type && $space->is_enemy()) {
                return true;
            }
            if ('archer' == $type && is_a($space->unit(), 'PHPWarrior\Units\Archer')) {
                return true;
            }
            if ('captive' == $type && is_a($space->unit(), 'PHPWarrior\Units\Captive')) {
                return true;
            }
            if (!$space->is_empty() || $space->is_wall()) {
                return false;
            }
        }

        return false;
    }

    protected function should_rest(): bool
    {
        return $this->warrior->health() < self::MIN_HP || $this->resting;
    }

    protected function taking_damage(): bool
    {
        return $this->warrior->health() < $this->health;
    }
}
