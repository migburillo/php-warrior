<?php

class Player
{
    protected const DIRECTIONS           = ['forward', 'right', 'backward', 'left'];
    protected const MAX_HP               = 17;
    protected const MIN_HP               = 12;
    protected const BOMB_SURROUNDING_DMG = 4;

    /**
     * @var array
     */
    protected $directions_to_check;

    /**
     * @var bool
     */
    protected $resting = false;

    /**
     * @var int
     */
    protected $health;

    /**
     * @var bool
     */
    protected $bound_enemy = false;

    /**
     * @var bool
     */
    protected $self_damaged = false;

    public function __construct()
    {
        $this->directions_to_check = self::DIRECTIONS;
    }

    public function play_turn($warrior)
    {
        $this->warrior = $warrior;

        if ($this->should_rest() && !$this->taking_damage() && $this->enemies_left()) {
            if (!$this->get_ticking_direction() || $warrior->health() <= self::BOMB_SURROUNDING_DMG || $this->resting) {
                $warrior->rest();
                $this->resting = !($warrior->health() >= self::MAX_HP);

                return;
            }
        }

        $this->health       = $warrior->health();
        $this->self_damaged = false;

        if ($direction = $this->get_bind_direction(self::DIRECTIONS)) {
            $this->bound_enemy = true;
            $this->warrior->bind($direction);

            return;
        }

        if ($direction = $this->get_ticking_direction()) {
            if ($this->warrior->feel($direction)->is_enemy()) {
                $this->attack($direction);
            } elseif ($this->warrior->feel($direction)->is_captive()) {
                $this->warrior->rescue($direction);
            } else {
                $warrior->walk($direction);
            }

            return;
        }

        if ($this->check_surroundings()) {
            return;
        }

        $this->directions_to_check = self::DIRECTIONS;
        $warrior->walk($this->get_direction_to_go());
    }

    protected function attack(string $direction)
    {
        if ($this->two_enemies_ahead($direction) && $this->warrior->health() > self::BOMB_SURROUNDING_DMG) {
            $this->warrior->detonate($direction);
            $this->self_damaged = true;
        } else {
            $this->warrior->attack($direction);
        }
    }

    protected function two_enemies_ahead(string $direction): bool
    {
        $spaces = $this->warrior->look($direction);

        return count($spaces) > 1 && $spaces[0]->is_enemy() && $spaces[1]->is_enemy();
    }

    protected function get_ticking_direction(): ?string
    {
        foreach ($this->warrior->listen() as $space) {
            if (!$space->is_ticking()) {
                continue;
            }

            return $this->warrior->direction_of($space);
        }

        return null;
    }

    /**
     * Returns true if there are enemies or captives that may be enemies.
     */
    protected function enemies_left(): bool
    {
        $captivesCount = 0;
        foreach ($this->warrior->listen() as $space) {
            if ($space->is_enemy()) {
                return true;
            }
            if ($space->is_captive()) {
                ++$captivesCount;
            }
        }

        return $this->bound_enemy && $captivesCount;
    }

    protected function get_direction_to_go(): string
    {
        $spaces = $this->warrior->listen();
        if ($spaces = $this->warrior->listen()) {
            $direction = $this->warrior->direction_of($spaces[0]);

            if ($this->warrior->feel()->is_stairs()) {
                $direction = 'left';
            }

            return $direction;
        }

        return  $this->warrior->direction_of_stairs();
    }

    protected function get_bind_direction(array $directions): ?string
    {
        $enemiesCount  = 0;
        $bindDirection = null;

        $ticking_direction = $direction = $this->get_ticking_direction();
        foreach ($directions as $direction) {
            if ($this->warrior->feel($direction)->is_enemy()) {
                ++$enemiesCount;
                if ($ticking_direction == $direction) {
                    continue;
                }
                $bindDirection = $direction;
            }
        }

        return $enemiesCount > 1 ? $bindDirection : null;
    }

    protected function check_surroundings(): bool
    {
        if (!$direction = $this->directions_to_check[0] ?? null) {
            return false;
        }

        if ($this->warrior->feel($direction)->is_enemy()) {
            $this->warrior->attack($direction);

            return true;
        }

        if ($this->warrior->feel($direction)->is_captive()) {
            $this->warrior->rescue($direction);

            return true;
        }

        array_shift($this->directions_to_check);

        return $this->check_surroundings();
    }

    protected function should_rest(): bool
    {
        return $this->warrior->health() < self::MIN_HP || $this->resting;
    }

    protected function taking_damage(): bool
    {
        return $this->warrior->health() < $this->health && !$this->self_damaged;
    }
}
