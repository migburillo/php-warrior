<?php

include 'player.php';

use Mocks\SpaceMock;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testPlayTurn(array $warriorInput, array $expected)
    {
        $player  = new Player();
        $warrior = new \Mocks\WarriorMock($warriorInput);

        $player->play_turn($warrior);

        $this->assertEquals($expected['restInvoked'] ?? false, $warrior->restInvoked, 'restInvoked');
        $this->assertEquals($expected['bindInvoked'] ?? false, $warrior->bindInvoked, 'bindInvoked');
        $this->assertEquals($expected['rescueInvoked'] ?? false, $warrior->rescueInvoked, 'rescueInvoked');
        $this->assertEquals($expected['walkInvoked'] ?? false, $warrior->walkInvoked, 'walkInvoked');
    }

    public function getTestData()
    {
        return [
            [
                'warriorInput' => ['feelSpace' => new SpaceMock(['isEnemy' => true])],
                'expected'     => ['bindInvoked' => true],
            ],
            [
                'warriorInput' => ['feelSpace' => new SpaceMock(['isEnemy' => true])],
                'expected'     => ['bindInvoked' => true],
            ],
            [
                'warriorInput' => ['feelSpace' => new SpaceMock(['isCaptive' => true, 'isTicking' => true])],
                'expected'     => ['rescueInvoked' => true],
            ],
            [
                'warriorInput' => ['feelSpace' => new SpaceMock(['isEnemy' => true, 'isTicking' => true])],
                'expected'     => ['bindInvoked' => true],
            ],
            [
                'warriorInput' => ['feelSpace' => new SpaceMock()],
                'expected'     => ['walkInvoked' => true],
            ],
            [
                'warriorInput' => [
                    'health'    => 5,
                    'feelSpace' => new SpaceMock(['isTicking' => true]),
                ],
                'expected' => ['walkInvoked' => true],
            ],
            [
                'warriorInput' => [
                    'health'    => 5,
                    'feelSpace' => new SpaceMock(['isEnemy' => true, 'isTicking' => true]),
                ],
                'expected' => ['bindInvoked' => true],
            ],
            [
                'warriorInput' => [
                    'health'    => 5,
                    'feelSpace' => new SpaceMock(['isEnemy' => true]),
                ],
                'expected' => ['restInvoked' => true],
            ],
        ];
    }
}
